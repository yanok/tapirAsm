module Utils

import Data.Vect
import Data.HVect

{-
public export
unrollArgs : List Type -> Type -> Type
unrollArgs [] b = b
unrollArgs (a :: as) b = a -> unrollArgs as b

public export
applyHVect : (unrollArgs as b) -> HVect (fromList as) -> b
applyHVect {as = []} b [] = b
-- Idris is unable to see that v is a cons
-- investigate later
-- most probably fromList is opaque
applyHVect {as = (x :: xs)} f v = ?applyHVect_rhs_2
-}

public export
getJust : (x : Maybe a) -> IsJust x -> a
getJust (Just p) ItIsJust = p

namespace Vectors
  public export
  vectToHVect : (tmap : a -> Type) -> ((x : a) -> tmap x) -> (xs : Vect k a) -> HVect (map tmap xs)
  vectToHVect tmap f [] = []
  vectToHVect tmap f (x :: xs) = f x :: vectToHVect tmap f xs

  public export
  hVectToVect : (tmap : a -> Type) -> (xs : Vect k a) -> HVect (map tmap xs)
             -> (f : {x : a} -> tmap x -> b) -> Vect k b
  hVectToVect tmap [] [] f = []
  hVectToVect tmap (t :: ts) (x :: xs) f = f x :: hVectToVect tmap ts xs f

  public export
  unrollArgs : Vect k Type -> Type -> Type
  unrollArgs [] b = b
  unrollArgs (a :: as) b = a -> unrollArgs as b

  public export
  applyHVect : (unrollArgs as b) -> HVect as -> b
  applyHVect {as = []} b [] = b
  applyHVect {as = (x :: xs)} f (v :: vs) = applyHVect (f v) vs
