module Tapir.Instruction

import Data.HVect

import Tapir.Type
import Tapir.Function.Signature
import Tapir.Repr
import Tapir.Proof
import Tapir.Value
import Tapir.Arch
import Tapir.Focus
import Tapir.Effect
import Utils

%default total

public export
writeEffect : Operand r (TRef ty) -> List (Effect r)
writeEffect (Reg _) = []
writeEffect (Ref ref) = [EUpdate ref]

public export
data Instruction : (r : Repr)
                 -> (arch : Arch r)
                 -> (effs : List (Effect r))
                 -> (s : Nat)
                 -> (s' : Nat)
                 -> (st : Nat)
                 -> (st' : Nat)
                 -> Type
                 -> Type where
     Add : {sz : ISize} ->
           Operand r (TInt sz) ->
           Operand r (TInt sz) ->
           Instruction r arch [] s s (1 + st) st (Value r (TInt sz))
     Sub : {sz : ISize} ->
           Operand r (TInt sz) ->
           Operand r (TInt sz) ->
           Instruction r arch [] s s (1 + st) st (Value r (TInt sz))
     Mul : {sz : ISize} ->
           Operand r (TInt sz) ->
           Operand r (TInt sz) ->
           Instruction r arch [] s s (1 + st) st (Value r (TInt sz))
     Div : {sz : ISize} ->
           Operand r (TInt sz) ->
           (d : Operand r (TInt sz)) ->
           {auto prfNZ : IsNZ r d} ->
           Instruction r arch [] s s (1 + st) st (Value r (TInt sz))
     And : {sz : ISize} ->
           Operand r (TInt sz) ->
           Operand r (TInt sz) ->
           Instruction r arch [] s s (1 + st) st (Value r (TInt sz))
     Or  : {sz : ISize} ->
           Operand r (TInt sz) ->
           Operand r (TInt sz) ->
           Instruction r arch [] s s (1 + st) st (Value r (TInt sz))
     BAnd : {sz : ISize} ->
            Operand r (TInt sz) ->
            Operand r (TInt sz) ->
            Instruction r arch [] s s (1 + st) st (Value r (TInt sz))
     BOr : {sz : ISize} ->
           Operand r (TInt sz) ->
           Operand r (TInt sz) ->
           Instruction r arch [] s s (1 + st) st (Value r (TInt sz))
     BXor : {sz : ISize} ->
            Operand r (TInt sz) ->
            Operand r (TInt sz) ->
            Instruction r arch [] s s (1 + st) st (Value r (TInt sz))
     BNeg : {sz : ISize} ->
            Operand r (TInt sz) ->
            Instruction r arch [] s s (1 + st) st (Value r (TInt sz))
     LShift : {sz : ISize} ->
              Operand r (TInt sz) ->
              Operand r (TInt sz) ->
              Instruction r arch [] s s (1 + st) st (Value r (TInt sz))
     RShift : {sz : ISize} ->
              Operand r (TInt sz) ->
              Operand r (TInt sz) ->
              Instruction r arch [] s s (1 + st) st (Value r (TInt sz))
     CastTo : {sz : ISize} -> -- TODO: add safer Expand
              (sz' : ISize) ->
              Operand r (TInt sz) ->
              Instruction r arch [] s s st st (Value r (TInt sz'))
     Peek : (sz : ISize) ->
            (p : Operand r (Word arch)) ->
            {auto prfSP : IsSafePtr arch p} ->
            Instruction r arch [] s s (1 + st) st (Value r (TInt sz))
     ReadRef : Operand r (TRef t) ->
               Focus r t t' ->
               Instruction r arch [] s s (opSize t' + st) st (Value r t')
     WriteRef : (ref : Operand r (TRef t)) ->
                Focus r t t' ->
                Operand r t' ->
                Instruction r arch (writeEffect ref) s s (opSize t' + st) st ()
     Eql : (x : Operand r (TInt sz)) ->
           (y : Operand r (TInt sz)) ->
           Instruction r arch [] (1 + s) s (1 + st) st (BoolReg r (EQL x y))
     Lt : (x : Operand r (TInt sz)) ->
          (y : Operand r (TInt sz)) ->
          Instruction r arch [] (1 + s) s (1 + st) st (BoolReg r (LT x y))
     Forget : BoolReg r prop -> Instruction r arch [] s s' st st' (Value r (TInt ISZ8))
     Br : BoolReg r prop ->
          (Proof r prop -> Instruction r arch effs s s' st st' (Value r ty)) ->
          (Proof r (NOT prop) -> Instruction r arch effs s s' st st' (Value r ty)) ->
          Instruction r arch effs (size ty + s) s' (1 + st) st' (reg r ty)
     BLoop : (n' : Operand r (TInt ISZ64)) ->
             {auto prfN : Proof r (EQL n' (Imm64 (fromInteger $ cast n)))} ->
             Operand r ty ->
             (body : {s'' : Nat} ->
                     {st' : Nat} ->
                     (cnt : reg r (TInt ISZ64)) ->
                     Proof r (LT (Reg cnt) (Imm64 (fromInteger $ cast n))) ->
                     (acc : reg r ty) ->
                     (h : hole r acc) ->
                     Instruction r arch effs (s' + s'') s'' (st + st') st' (Value r ty)) ->
             Instruction r arch effs (size ty + size (TInt ISZ64) + s' + s)
                                (size (TInt ISZ64) + s' + s)
                                (n*st + st')
                                st'
                                (reg r ty)
     Break : {rg : reg r ty} ->
             Value r ty ->
             hole r rg ->
             Instruction r arch [] s s (1 + st) st ()
     Alloca : {t : TType} ->
                Value r t ->
                Instruction r arch [] (size t + s) s st st (reg r t)
     Call : {sig : FunctionSignature r} ->
            fun r sig ->
            (p : paramType sig) ->
            (args : HVect (map (Operand r) (arguments sig p))) ->
            {auto prfs : HVect (map (Proof r) (applyHVect (proofs sig p) args))} ->
            Instruction r arch (applyHVect (effects sig p) args)
                  (stackCost sig p + size (returnType sig p) + s) s
                  (stepCost sig p + st) st
                  (reg r (returnType sig p))
     Wait : (n : Nat) -> Instruction r arch [] s s (n + st) st ()
     Waste : (n : Nat) -> Instruction r arch [] (n + s) s st st ()
     -- monad-like stuff
     (>>=) : Instruction r arch effs1 s s' st st' t ->
             (t -> Instruction r arch effs2 s' s'' st' st'' t') ->
             Instruction r arch (effs1 ++ effs2) s s'' st st'' t'
     Pure : a -> Instruction r arch [] s s t t a

public export
regOp : Instruction r arch [] (size t + s) (size t + s') st st' (Value r t) ->
        Instruction r arch [] (size t + s) s' st st' (reg r t)
regOp i = i >>= Alloca

public export
add : Operand r (TInt sz) -> Operand r (TInt sz) ->
      Instruction r arch [] (size (TInt sz) + s) s (1 + st) st (reg r (TInt sz))
add x y = regOp (Add x y)

public export
eql : (x : Operand r (TInt sz)) -> (y : Operand r (TInt sz)) ->
      Instruction r arch [] (1 + s) s (1 + st) st (BoolReg r (EQL x y))
eql x y = Eql x y

public export
lt : (x : Operand r (TInt sz)) -> (y : Operand r (TInt sz)) ->
     Instruction r arch [] (1 + s) s (1 + st) st (BoolReg r (LT x y))
lt x y = Lt x y

public export
div : Operand r (TInt sz) -> (d : Operand r (TInt sz)) ->
      {auto prfNZ : IsNZ r d} ->
      Instruction r arch [] (size (TInt sz) + s) s (1 + st) st (reg r (TInt sz))
div n d = Div n d >>= Alloca

public export
peek : (sz : ISize) ->
       (p : Operand r (Word arch)) -> {auto prfSP : IsSafePtr arch p} ->
       Instruction r arch [] (size (TInt sz) + s) s (1 + st) st (reg r (TInt sz))
peek sz p = Peek sz p >>= Alloca

syntax br [c] [thn] [els] = Br c (\_ => thn) (\_ => els)
