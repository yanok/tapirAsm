module Tapir.CCodeGen

import Control.Monad.State

import Tapir.Type
import Tapir.Repr
import Tapir.Proof
import Tapir.Value
import Tapir.Arch
import Tapir.Instruction
import Tapir.Module
import Tapir.FFI
import Tapir.Effect
import MiniC.Syntax

import Tapir.CCodeGen.CGRepr
import Tapir.CCodeGen.Monad
import Tapir.CCodeGen.Instruction
import Tapir.CCodeGen.Module

export
codegenInstruction : Instruction CGRepr arch effs s s' st st' () -> CBlock
codegenInstruction is
  = getBlock $ execState (cgInstruction is pure) emptyCGState

codegenModule' : Module CGRepr arch ffi () -> CModule
codegenModule' p = flip evalState emptyCGState $
  cgModule p (\_ => do
    mds <- gets getModuleDefs
    (ds, _) <- gets getBlock
    pure $ mds ++ ds)

export
codegenModule : {arch : (repr : Repr) -> Arch repr}
             -> {ffi : (repr : Repr) -> FFI repr}
             -> ({repr : Repr} -> Module repr (arch repr) (ffi repr) ())
             -> CModule
codegenModule mod = codegenModule' mod

