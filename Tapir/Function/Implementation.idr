module Tapir.Function.Implementation

import Data.HVect

import Tapir.Type
import Tapir.Repr
import Tapir.Arch
import Tapir.Function.Signature
import Tapir.Instruction
import Tapir.Effect
import Tapir.Proof
import Tapir.Value
import Utils

public export
record FunctionImpl
       (r : Repr)
       (arch : Arch r)
       (sig : FunctionSignature r) where
  constructor MkFunctionImpl
  functionBody :
               {st, s : Nat} ->
               (p : paramType sig) ->
               (args : HVect (map (Operand r) (arguments sig p))) ->
               (prfs : HVect (map (Proof r) (applyHVect (proofs sig p) args))) ->
               Instruction r arch
                           (applyHVect (effects sig p) args)
                           (stackCost sig p + s) s
                           (stepCost sig p + st) st
                           (Value r (returnType sig p))
