module Tapir.Function.Signature

import Data.Vect

import Tapir.Type
import Tapir.Effect
import Utils

public export
record FunctionSignature' (reg : TType -> Type) (ref : TType -> Type) where
  constructor MkFunSig
  paramType : Type
  returnType : paramType -> TType
  arguments : paramType -> Vect k TType
  -- TODO: proofs of runtime representation of returnType and arguments being
  -- independent of a passed parameter (otherwise we won't be able to compile it)
  proofs : (p : paramType) ->
           unrollArgs (map (Operand' reg ref) (arguments p)) (Vect kp (Prop' reg ref))
  stackCost : paramType -> Nat
  stepCost : paramType -> Nat
  effects : (p : paramType) ->
            unrollArgs (map (Operand' reg ref) (arguments p)) (List (Effect' reg ref))
