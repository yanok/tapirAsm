module Tapir.Repr

import Tapir.Type
import Tapir.Effect
import Tapir.Function.Signature

public export
record Repr where
  constructor MkRepr
  reg  : TType -> Type
  ref  : TType -> Type
  val  : TType -> Type
  prf  : Prop' reg ref -> Type
  breg : Prop' reg ref -> Type
  hole : {ty : TType} -> reg ty -> Type
  fun  : FunctionSignature' reg ref -> Type

public export
Operand : (r : Repr) -> TType -> Type
Operand r = Operand' (reg r) (ref r)

public export
Prop : (r : Repr) -> Type
Prop r = Prop' (reg r) (ref r)

public export
FunctionSignature : (r : Repr) -> Type
FunctionSignature r = FunctionSignature' (reg r) (ref r)

public export
Effect : (r : Repr) -> Type
Effect r = Effect' (reg r) (ref r)
