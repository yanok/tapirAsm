module Tapir.Proof

import public Data.So

import Tapir.Type
import Tapir.Repr

%default total

public export
data Proof : (r : Repr) -> Prop r -> Type where
     LTProof8 : So (x < y) -> Proof r (LT (Imm8 x) (Imm8 y))
     LTProof16 : So (x < y) -> Proof r (LT (Imm16 x) (Imm16 y))
     LTProof32 : So (x < y) -> Proof r (LT (Imm32 x) (Imm32 y))
     LTProof64 : So (x < y) -> Proof r (LT (Imm64 x) (Imm64 y))
     EQLProof8 : So (x == y) -> Proof r (EQL (Imm8 x) (Imm8 y))
     EQLProof16 : So (x == y) -> Proof r (EQL (Imm16 x) (Imm16 y))
     EQLProof32 : So (x == y) -> Proof r (EQL (Imm32 x) (Imm32 y))
     EQLProof64 : So (x == y) -> Proof r (EQL (Imm64 x) (Imm64 y))
     NEQLProof8 : So (x /= y) -> Proof r (NOT (EQL (Imm8 x) (Imm8 y)))
     NEQLProof16 : So (x /= y) -> Proof r (NOT (EQL (Imm16 x) (Imm16 y)))
     NEQLProof32 : So (x /= y) -> Proof r (NOT (EQL (Imm32 x) (Imm32 y)))
     NEQLProof64 : So (x /= y) -> Proof r (NOT (EQL (Imm64 x) (Imm64 y)))
     Prf : prf r prop -> Proof r prop

public export
data IsNZ : (r : Repr) -> Operand r (TInt sz) -> Type where
     NZProof8 : Proof r (NOT (EQL x (Imm8 0))) -> IsNZ r x
     NZProof16 : Proof r (NOT (EQL x (Imm16 0))) -> IsNZ r x
     NZProof32 : Proof r (NOT (EQL x (Imm32 0))) -> IsNZ r x
     NZProof64 : Proof r (NOT (EQL x (Imm64 0))) -> IsNZ r x

public export
data BoolReg : (r : Repr) -> Prop r -> Type where
     BTrue   : Proof r prop -> BoolReg r prop
     BFalse  : Proof r (NOT prop) -> BoolReg r prop
     BReg    : breg r prop -> BoolReg r prop
