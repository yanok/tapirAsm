module Tapir.Arch.X86_64

import Tapir.Type
import Tapir.Repr
import Tapir.Proof
import Tapir.Arch

public export
record X8664SysInfo  where
  constructor MkX8664SysInfo
  memHighBound, memLowBound : Bits64

public export
data X8664IsSafePtr : (r : Repr)
                   -> (si : X8664SysInfo)
                   -> Operand r (TInt ISZ64)
                   -> Type where
     SafePtr : Proof r (LT p (Imm64 (memHighBound si))) ->
               Proof r (LT (Imm64 (memLowBound si)) p) -> X8664IsSafePtr r si p

public export
x8664PArch : PArch r
x8664PArch {r} = MkPArch
      X8664SysInfo
      (\si => MkArch ISZ64 (X8664IsSafePtr r si))

-- public export
-- x86withSafePtr : (p : Operand stage (Word Archs.X86_64.arch)) ->
--                  ((prf : IsSafePtr Archs.X86_64.arch p) ->
--                          Instr Archs.X86_64.arch stage s s' st st' a) ->
--                  Instr Archs.X86_64.arch stage s s' st st' a ->
--                  Instr Archs.X86_64.arch stage (2 + s) s' (4 + st) st' a
-- x86withSafePtr p prg fail = do
--   ch <- lt p (@ HighBound)
--   cl <- lt (@ LowBound) p
--   Br ch
--      (\ph => Br cl
--                 (\pl => prg (SafePtr ph pl))
--                 (\_ => fail))
--      (\_ => do Wait 1
--                fail)

-- archLib : ArchLib Archs.X86_64.arch stage
-- archLib = MkArchLib 4 2 x86withSafePtr
