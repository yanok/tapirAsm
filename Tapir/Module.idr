module Tapir.Module

import Data.Vect
import Data.HVect

import Tapir.Type
import Tapir.Repr
import Tapir.Arch
import Tapir.Function.Signature
import Tapir.Function.Implementation
import Tapir.FFI
import Tapir.CInterop.CName
import Tapir.Instruction
import Tapir.Effect
import Tapir.Value
import Tapir.Proof
import Utils

public export
data Module : (r : Repr)
           -> (arch : Arch r)
           -> (ffi : FFI r)
           -> Type
           -> Type where
     GetFFI : Module r arch ffi
              ( HVect $ map (\s => fun r (snd s)) (functions ffi)
              , HVect $ map (\g => reg r (snd g)) (globals ffi))
     Data : (name : String) ->
            (ty : TType) ->
            Module r arch ffi (ref r ty)
     Func : FunctionImpl r arch sig ->
            Module r arch ffi (fun r sig)
     JProbe : (name : String) ->
              (args : Vect k (CName arch)) ->
              (effs : List (Effect r)) ->
              (slots : Nat) ->
              (steps : Nat) ->
              unrollArgs (map (CNameRep r arch) args)
                         (Instruction r arch effs slots 0 steps 0 ()) ->
              Module r arch ffi ()
     AllocEP : (name : String) -> (ty : TType) ->
               Module r arch ffi (reg r (TOpaque "tapirEP" [ty]))
     --
     Pure : a -> Module r arch ffi a
     (>>=) : Module r arch ffi a ->
             (a -> Module r arch ffi b) ->
             Module r arch ffi b
