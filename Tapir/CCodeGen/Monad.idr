module Tapir.CCodeGen.Monad

import Control.Monad.State
import Data.SortedMap
import Data.Vect
import Prelude.Pair.Extra

import MiniC.Syntax
import Tapir.Type
import Tapir.Proof
import Tapir.Repr
import Tapir.CCodeGen.CGRepr
import Tapir.CCodeGen.Basic

%default total
%access export

export
record CGState where
  constructor MkCGState
  reg        : Nat
  block      : CBlock
  lbl        : Nat
  struct     : Nat
  structMap  : SortedMap (List (String, CType)) String
  moduleDefs : List CDef

export
emptyCGState : CGState
emptyCGState = MkCGState 0 neutral 0 0 empty neutral

export
getBlock : CGState -> CBlock
getBlock = block

export
getModuleDefs : CGState -> List CDef
getModuleDefs = moduleDefs

public export
CG : (a : Type) -> Type
CG a = State CGState a

newLabel : String -> CG String
newLabel str = do
         n <- gets lbl
         modify $ record {lbl $= (+ 1)}
         pure $ str ++ show n

newRegNum : CG Nat
newRegNum = do
          r <- gets reg
          modify $ record {reg $= (+ 1)}
          pure r

newStructNum : CG Nat
newStructNum = do
  n <- gets struct
  modify $ record {struct $= (+ 1)}
  pure n

emitModDef : CDef -> CG ()
emitModDef def = modify $ record {moduleDefs $= (<+> [def])}

findStruct : (fs : List (String, CType)) -> CG CType
findStruct fs = do
  sm <- gets structMap
  Nothing <- pure $ lookup fs sm
      | Just s => pure (CStruct s)
  n <- newStructNum
  let name = "user_struct_" ++ show n
  emitModDef $ CStructDef fs name
  modify $ record {structMap $= insert fs name}
  pure $ CStruct name

emitDef : CDef -> CG ()
emitDef def = modify $ record {block $= (<+> ([def], []))}

emitStmt : CStmt -> CG ()
emitStmt stmt = modify $ record {block $= (<+> ([], [(Nothing , stmt)]))}

emitLabel : String -> CG ()
emitLabel l = modify $ record {block $= (<+> ([], [(Just l, CDiscard (CIntLit 0))]))}

-- unfortunately now cgType has to be here
cgType : TType -> CG CType
cgType (TInt x) = pure $ CUnsigned (cgSz x)
cgType (TRef x) = CPtr <$> cgType x
cgType (TArray ty n) = CArray <$> cgType ty <*> pure n
cgType (TRecord fs) = do
  -- don't want to inline traverse, so assert_total
  fs' <- assert_total $ traverse (traverse cgType) fs
  findStruct $ toList fs'
cgType (TOpaque n _) = pure $ CTypedef n

newReg : (ty : TType) -> CG (Register ty)
newReg ty = do
       r <- newRegNum
       cty <- cgType ty
       emitDef $ CVarDef (ppRegNum r) cty Nothing
       pure $ LReg r

newBoolReg : (prop : Prop CGRepr) -> CG (BoolReg CGRepr prop)
newBoolReg prop = do
           r <- newRegNum
           emitDef $ CVarDef (ppRegNum r) (CUnsigned CSZ8) Nothing
           pure $ BReg $ BLReg r

makeBlock : CG a -> CG (a, CBlock)
makeBlock a = do
  old <- gets block
  modify $ record { block = neutral }
  v <- a
  r <- gets block
  modify $ record { block = old }
  pure (v, r)

makeBlock_ : CG () -> CG CBlock
makeBlock_ a = snd <$> makeBlock a
