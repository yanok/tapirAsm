module Tapir.CCodeGen.CGRepr

import Tapir.Type
import Tapir.Function.Signature
import Tapir.Repr
import MiniC.Syntax

%default total
%access public export

data Register : TType -> Type where
  LReg  : Nat -> Register ty
  STReg : String -> Register ty

data CGRef : TType -> Type where
  STRef : String -> CGRef ty

data BRegister : Prop' reg ref -> Type where
  BLReg : Nat -> BRegister prop

record Hole (r : Register ty) where
  constructor MkHole
  hLabel : String

record Function (sig : FunctionSignature' reg ref) where
  constructor MkFunction
  funName : String

CGRepr : Repr
CGRepr = MkRepr Register CGRef (\_ => CExp) (\_ => Unit) BRegister Hole Function
