module Tapir.CCodeGen.Instruction

import Control.Monad.State
import Data.HVect

import MiniC.Syntax
import Tapir.Type
import Tapir.Proof
import Tapir.Value
import Tapir.Arch
import Tapir.Function.Signature
import Tapir.Repr
import Tapir.Instruction
import Tapir.Focus
import Tapir.Effect
import Utils

import Tapir.CCodeGen.CGRepr
import Tapir.CCodeGen.Basic
import Tapir.CCodeGen.Monad
import Tapir.CCodeGen.Access
import Tapir.CCodeGen.BLoop
import Tapir.CCodeGen.Branch

%default total

export
cgInstruction : Instruction CGRepr arch effs s s' st st' a ->
                (a -> CG b) ->
                CG b
cgInstruction (Add x y) k = k $ Val (CAdd (cgOp x) (cgOp y))
cgInstruction (Sub x y) k = k $ Val (CSub (cgOp x) (cgOp y))
cgInstruction (Mul x y) k = k $ Val (CMul (cgOp x) (cgOp y))
cgInstruction (Div x d) k = k $ Val (CDiv (cgOp x) (cgOp d))
cgInstruction (And x y) k = k $ Val (CAnd (cgOp x) (cgOp y))
cgInstruction (Or x y) k = k $ Val (COr (cgOp x) (cgOp y))
cgInstruction (BAnd x y) k = k $ Val (CBAnd (cgOp x) (cgOp y))
cgInstruction (BOr x y) k = k $ Val (CBOr (cgOp x) (cgOp y))
cgInstruction (BXor x y) k = k $ Val (CBXor (cgOp x) (cgOp y))
cgInstruction (BNeg x) k = k $ Val (CBNeg (cgOp x))
cgInstruction (LShift x y) k = k $ Val (CLShift (cgOp x) (cgOp y))
cgInstruction (RShift x y) k = k $ Val (CRShift (cgOp x) (cgOp y))
cgInstruction (CastTo sz x) k = k $ Val (CCast (CUnsigned $ cgSz sz) (cgOp x))
cgInstruction (Peek sz p) k
   = k $ Val (CDeref (CUnsigned (cgSz sz)) (cgOp p))
cgInstruction (ReadRef r l) k = cgReadRef r l >>= k
cgInstruction (WriteRef x l v) k = cgWriteRef x l v >>= k
cgInstruction (Eql x y) k = do
   r <- newBoolReg (EQL x y)
   emitStmt (CAssign (cgBoolReg r) (CEql (cgOp x) (cgOp y)))
   k r
cgInstruction (Lt x y) k = do
   r <- newBoolReg (LT x y)
   emitStmt (CAssign (cgBoolReg r) (CLt (cgOp x) (cgOp y)))
   k r
cgInstruction (Br x f g) k = assert_total $ cgBr cgInstruction x f g >>= k
cgInstruction (BLoop n init body) k
   = assert_total $ cgBLoop cgInstruction n init body >>= k
cgInstruction (Break v (MkHole {r} l)) k = do
   cgAssign r v
   emitStmt $ CGoto l
   k ()
cgInstruction (Alloca (ValInt {sz} i)) k = do
   r <- newReg (TInt sz)
   emitStmt (CAssign (cgReg r) (CIntLit i))
   k r
cgInstruction (Alloca (VOp {ty} op)) k = do
   r <- newReg ty
   emitStmt (CAssign (cgReg r) (cgOp op))
   k r
cgInstruction (Alloca {t} v) k = do
   r <- newReg t
   cgAssign r v
   k r
cgInstruction (Forget b) k = do
   r <- newReg (TInt ISZ8)
   emitStmt (CAssign (cgReg r) (cgBoolReg b))
   k (VOp (% r))
cgInstruction (Call {sig} (MkFunction f) p as) k = do
   let as' = hVectToVect _ _ as cgArg
   r <- newReg (returnType sig p)
   emitStmt $ CAssign (cgReg r) (CFunCall f $ toList as')
   k r
cgInstruction ((>>=) x f) k = cgInstruction x (\r => cgInstruction (f r) k)
cgInstruction (Pure x) k = k x
cgInstruction (Wait _) k = k ()
cgInstruction (Waste _) k = k ()
