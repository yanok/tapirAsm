module Tapir.CCodeGen.Block

import Control.Monad.State
import Data.Vect

import MiniC.Syntax
import Tapir.Type
import Tapir.Proof
import Tapir.Value
import Tapir.Arch
import Tapir.Instruction
import Tapir.Repr
import Tapir.Effect

import Tapir.CCodeGen.CGRepr
import Tapir.CCodeGen.Basic
import Tapir.CCodeGen.Monad

%default total

export
cgBlock : (cg : {a, b : Type} ->
                {s : Nat} ->
                {s' : Nat} ->
                {st : Nat} ->
                {st' : Nat} ->
                Instruction CGRepr arch effs s s' st st' a ->
                (a -> CG b) ->
                CG b) ->
          Instruction CGRepr arch effs s s' st st' a ->
          (a -> CG ()) ->
          CG CBlock
cgBlock cg is fin = makeBlock_ $ cg is fin
