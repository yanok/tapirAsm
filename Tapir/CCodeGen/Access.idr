module Tapir.CCodeGen.Access

import Control.Monad.State
import Data.Vect

import MiniC.Syntax
import Tapir.Type
import Tapir.Proof
import Tapir.Value
import Tapir.Focus
import Tapir.Repr

import Tapir.CCodeGen.CGRepr
import Tapir.CCodeGen.Basic
import Tapir.CCodeGen.Monad

%default total

mutual
  cgAssignArray : Nat -> CExp -> Vect n (Value CGRepr ty) -> CG ()
  cgAssignArray i a [] = pure ()
  cgAssignArray i a (v :: vs) = do
     cgAssign' (CIndex a (CIntLit $ toIntegerNat i)) v
     cgAssignArray (S i) a vs

  cgAssign' : CExp -> Value CGRepr ty -> CG ()
  cgAssign' {ty = ty@(TInt sz)} l v@(ValInt x) = emitStmt $ CAssign l (cgVal {t = ty} v)
  cgAssign' {ty = ty} l v@(Val x) = emitStmt $ CAssign l (cgVal {t = ty} v)
  cgAssign' l (VOp op) = emitStmt $ CAssign l (cgOp op)
  cgAssign' l (VArr vs) = cgAssignArray 0 l vs
  cgAssign' l VRecNil = pure ()
  cgAssign' l (VRecCons nm v vs) = do
    cgAssign' (CField l nm) v
    cgAssign' l vs

export
cgAssign : Register ty -> Value CGRepr ty -> CG ()
cgAssign r v = cgAssign' (cgReg r) v

addDeref : Bool -> TType -> CExp -> CG CExp
addDeref True t e = do
  ct <- cgType t
  pure $ CDeref ct e
addDeref False _ e = pure e

cgFocus : Bool -> CExp -> Focus CGRepr t t' -> CG (Bool, CExp)
cgFocus needDeref base FId = pure (needDeref, base)
cgFocus needDeref base (FCo x y)
  = do (nd', b') <- cgFocus needDeref base x
       cgFocus nd' b' y
cgFocus needDeref base (FEl x i y) = do
  (_,le) <- cgFocus needDeref base x
  pure (False , CIndex le (cgOp i))
cgFocus needDeref base (FFld {fs} x f) = do
  (nd', b') <- cgFocus needDeref base x
  b'' <- addDeref nd' (TRecord fs) b'
  pure (False, CField b'' f)

mutual
  cgWriteRefArray : TType -> Nat -> CExp -> CExp -> CG ()
  cgWriteRefArray t Z l r
    = cgWriteRef' t False (CIndex l (CIntLit 0)) (CIndex r (CIntLit 0))
  cgWriteRefArray t (S k) l r = do
    cgWriteRefArray t k l r
    let i = toIntegerNat k
    cgWriteRef' t False (CIndex l (CIntLit i)) (CIndex r (CIntLit i))

  cgWriteRefs : TType ->
                Vect n (String, TType) ->
                Bool ->
                CExp ->
                CExp ->
                CG ()
  cgWriteRefs rect [] needDeref l r = pure ()
  cgWriteRefs rect ((n, t) :: fs) needDeref l r = do
    l' <- addDeref needDeref rect l
    cgWriteRef' t False (CField l' n) (CField r n)
    cgWriteRefs rect fs needDeref l r

  cgWriteRef' : TType -> Bool -> CExp -> CExp -> CG ()
  cgWriteRef' t@(TInt sz) needDeref l r = do
    l' <- addDeref needDeref t l
    emitStmt $ CAssign l' r
  cgWriteRef' t@(TRef x) needDeref l r = do
    l' <- addDeref needDeref t l
    emitStmt $ CAssign l' r
  cgWriteRef' (TArray t k) needDeref l r = cgWriteRefArray t k l r
  cgWriteRef' (TRecord fs) needDeref l r = cgWriteRefs (TRecord fs) fs needDeref l r
  cgWriteRef' t@(TOpaque _ _) needDeref l r = do
    l' <- addDeref needDeref t l
    emitStmt $ CAssign l' r

export
cgWriteRef : Operand CGRepr (TRef t) ->
             Focus CGRepr t t' ->
             Operand CGRepr t' ->
             CG ()
cgWriteRef {t'} ref l v = do
    (nd, e) <- cgFocus True (cgOp ref) l
    cgWriteRef' t' nd e (cgOp v)

mutual
  cgReadRefs : TType ->
               (fs : Vect n (String, TType)) ->
               Bool ->
               CExp ->
               CG (Value CGRepr (TRecord fs))
  cgReadRefs rect [] needDeref r = pure VRecNil
  cgReadRefs rect ((nm, t) :: xs) needDeref r = do
    e <- addDeref needDeref rect r
    v <- cgReadRef' t False (CField e nm)
    vs <- cgReadRefs rect xs needDeref r
    pure $ VRecCons nm v vs

  cgReadRef' : (t : TType) -> Bool -> CExp -> CG (Value CGRepr t)
  cgReadRef' t@(TInt z) needDeref r = Val <$> addDeref needDeref t r
  cgReadRef' t@(TRef z) needDeref r = Val <$> addDeref needDeref t r
  cgReadRef' (TArray t k) needDeref r
    = VArr <$> traverse (\i => cgReadRef' t False (CIndex r (CIntLit $ cast i))) range
  cgReadRef' (TRecord fs) needDeref r
    = cgReadRefs (TRecord fs) fs needDeref r
  cgReadRef' t@(TOpaque _ _) needDeref r = Val <$> addDeref needDeref t r

export
cgReadRef : Operand CGRepr (TRef t) -> Focus CGRepr t t' -> CG (Value CGRepr t')
cgReadRef {t'} r l = do
  (nd, e) <- cgFocus True (cgOp r) l
  cgReadRef' t' nd e
