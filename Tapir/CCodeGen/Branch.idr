module Tapir.CCodeGen.Branch

import Control.Monad.State

import MiniC.Syntax
import Tapir.Type
import Tapir.Proof
import Tapir.Value
import Tapir.Arch
import Tapir.Instruction
import Tapir.Repr
import Tapir.Effect

import Tapir.CCodeGen.CGRepr
import Tapir.CCodeGen.Basic
import Tapir.CCodeGen.Monad
import Tapir.CCodeGen.Access
import Tapir.CCodeGen.Block

%default total

cgBranches : (cg : {a, b : Type} ->
                   {effs : List (Effect CGRepr)} ->
                   {s : Nat} ->
                   {s' : Nat} ->
                   {st : Nat} ->
                   {st' : Nat} ->
                   Instruction CGRepr arch effs s s' st st' a ->
                   (a -> CG b) ->
                   CG b) ->
             (t : Proof CGRepr prop -> Instruction CGRepr arch effs1 s s' st st' a) ->
             (e : Proof CGRepr (NOT prop) -> Instruction CGRepr arch effs2 s s' st st' a) ->
             (a -> CG ()) ->
             CG (CBlock, CBlock)
cgBranches cg t e f = do
     tb <- cgBlock cg (t $ Prf ()) f
     eb <- cgBlock cg (e $ Prf ()) f
     pure (tb, eb)

export
cgBr : (cg : {a, b : Type} ->
             {effs : List (Effect CGRepr)} ->
             {s : Nat} ->
             {s' : Nat} ->
             {st : Nat} ->
             {st' : Nat} ->
             Instruction CGRepr arch effs s s' st st' a ->
             (a -> CG b) ->
             CG b) ->
       (x : BoolReg CGRepr prop) ->
       (t : Proof CGRepr prop ->
            Instruction CGRepr arch effs1 s s' st st' (Value CGRepr ty)) ->
       (e : Proof CGRepr (NOT prop) ->
            Instruction CGRepr arch effs2 s s' st st' (Value CGRepr ty)) ->
       CG (Register ty)
cgBr {ty = ty} cg x t e = do
     r <- newReg ty
     (tb, eb) <- cgBranches cg t e (\v => cgAssign r v)
     emitStmt (CIf (cgBoolReg x) tb eb)
     pure r
