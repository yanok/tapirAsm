module Tapir.CCodeGen.Basic

import Data.Vect
import Data.Bits

import MiniC.Syntax
import Tapir.Type
import Tapir.Repr
import Tapir.Proof
import Tapir.Value

import Tapir.CCodeGen.CGRepr

%default total
%access export

ppRegNum : Nat -> String
ppRegNum r = "reg" ++ show r

cgReg : Register t -> CExp
cgReg (LReg x) = CVar $ ppRegNum x
cgReg (STReg s) = CVar s

cgOp : Operand CGRepr t -> CExp
cgOp (Imm8 x) = CIntLit $ bitsToInt' {n = 8} x
cgOp (Imm16 x) = CIntLit $ bitsToInt' {n = 16} x
cgOp (Imm32 x) = CIntLit $ bitsToInt' {n = 32} x
cgOp (Imm64 x) = CIntLit $ bitsToInt' {n = 64} x
cgOp (Reg r) = cgReg r
cgOp (Ref (STRef s)) = CVar s

cgArg : Operand CGRepr t -> CExp
cgArg (Reg {t = (TRecord xs)} r) = CAddrOf (cgReg r)
cgArg op = cgOp op

cgBoolReg : BoolReg CGRepr prop -> CExp
cgBoolReg (BTrue x) = CIntLit 1
cgBoolReg (BFalse x) = CIntLit 0
cgBoolReg (BReg (BLReg x)) = CVar $ ppRegNum x

cgSz : ISize -> CSize
cgSz ISZ8 = CSZ8
cgSz ISZ16 = CSZ16
cgSz ISZ32 = CSZ32
cgSz ISZ64 = CSZ64

cgVal : (v : Value CGRepr t) -> {auto prf : ValFitsExp v} -> CExp
cgVal (ValInt x) {prf = ValFits_ValInt} = CIntLit x
cgVal (VOp o) {prf = ValFits_Op} = cgOp o
cgVal (Val x) {prf = ValFits_Val} = x
