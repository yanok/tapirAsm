module Tapir.CCodeGen.BLoop

import Control.Monad.State

import MiniC.Syntax
import Tapir.Type
import Tapir.Proof
import Tapir.Value
import Tapir.Arch
import Tapir.Instruction
import Tapir.Repr
import Tapir.Effect

import Tapir.CCodeGen.CGRepr
import Tapir.CCodeGen.Basic
import Tapir.CCodeGen.Monad
import Tapir.CCodeGen.Access
import Tapir.CCodeGen.Block

%default total

-- funny Idris bug: if I use st' as body argument, Idris doesn't allow me
-- to specify it explicitly
export
cgBLoop : (cg : {a, b : Type} ->
                {effs : List (Effect CGRepr)} ->
                {s : Nat} ->
                {s' : Nat} ->
                {st : Nat} ->
                {st' : Nat} ->
                Instruction CGRepr arch effs s s' st st' a ->
                (a -> CG b) ->
                CG b) ->
          {n : Nat} ->
          (n' : Operand CGRepr (TInt ISZ64)) ->
          (init : Operand CGRepr ty) ->
          (body : {s'' : Nat} ->
                  {st'' : Nat} ->
                  (cnt : Register (TInt ISZ64)) ->
                  (prf : Proof CGRepr (LT (Reg cnt) (Imm64 (fromInteger $ cast n)))) ->
                  (acc : Register ty) ->
                  Hole acc ->
                  Instruction CGRepr arch effs (s + s'') s'' (st + st'') st''
                              (Value CGRepr ty)) ->
          CG (Register ty)
cgBLoop {ty} cg n' init body = do
        acc <- newReg ty
        emitStmt $ CAssign (cgReg acc) (cgOp init)
        r <- newReg (TInt ISZ64)
        emitStmt $ CAssign (cgReg r) (CIntLit 0)
        l <- newLabel "out_loop"
        bd <- cgBlock cg
                      (body {s'' = 0}{st'' = 0} r (Prf ()) acc (MkHole l))
           (\v => do
               emitStmt $ CAssign (cgReg r) (CAdd (cgReg r) (CIntLit 1))
               cgAssign acc v)
        emitStmt $ CWhile (CNeq (cgReg r) (cgOp n')) bd
        emitLabel l
        pure acc
