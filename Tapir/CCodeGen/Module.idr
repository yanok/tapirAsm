module Tapir.CCodeGen.Module

import Control.Monad.State
import Data.Vect
import Data.HVect

import Tapir.Arch
import Tapir.FFI
import Tapir.Function.Signature
import Tapir.Module
import Tapir.CInterop.CName
import Tapir.Type
import Tapir.Repr
import Tapir.Instruction
import Tapir.Effect
import Utils
import MiniC.Syntax

import Tapir.CCodeGen.CGRepr
import Tapir.CCodeGen.Basic
import Tapir.CCodeGen.Monad
import Tapir.CCodeGen.Instruction
import Tapir.CCodeGen.Block

%default total

export
cgModule : Module CGRepr arch ffi a -> (a -> CG CModule)
        -> CG CModule
cgModule {arch = arch} {ffi = (MkFFI functions globals)} GetFFI k
  = k ( vectToHVect (\s => Function (snd s))
                    (\s => MkFunction (fst s))
                    functions
      , vectToHVect (\g => Register (snd g))
                    (\g => STReg (fst g))
                    globals) -- TODO: do something with module defs?
cgModule (Data name ty) k = do
  ty' <- cgType ty
  emitDef $ CVarDef name ty' Nothing
  emitDef $ CVarDef (name ++ "_ref") (CPtr ty') (Just $ CAddrOf $ CVar name)
  k $ STRef $ name ++ "_ref"
cgModule {arch} (JProbe nm as _ _ _ f) k = do
  let asR = vectToHVect (\cn => CNameRep CGRepr arch cn)
                        (\cn => MkCNameRep (STReg (name cn))) -- Idris bug: composition doesn't work, see #3133
                        as
  b <- cgBlock cgInstruction (applyHVect f asR)
               (\_ => emitStmt $ CDiscard $ CFunCall "jprobe_return" [] )
  let asC = toList $ map (\cn => (name cn, cType cn)) as
  emitDef $ CFunDef CVoid ("jprobe_" ++ nm) asC b
  k ()
cgModule (AllocEP nm ty) k = do
  epTy <- cgType $ TOpaque "tapirEP" [ty]
  let epName = "ep_" ++ nm
  emitDef $ CVarDef epName epTy (Just $ CFunCall "ep_alloc" [CIntLit $ cast $ size ty])
  k (STReg epName)
cgModule (Pure x) k = k x
cgModule (y >>= f) k = cgModule y (\r => cgModule (f r) k)
