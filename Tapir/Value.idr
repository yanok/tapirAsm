module Tapir.Value

import Data.So
import Data.Vect

import Tapir.Type
import Tapir.Repr

public export
data Value : (r : Repr) -> TType -> Type where
     ValInt : Integer -> Value r (TInt sz)
     -- allow to make Values from things already available as Operands
     -- TODO: this should probably replace ValInt constructor
     VOp : Operand r ty -> Value r ty
     -- allow to represent values using val from Repr
     -- may be used for code generation
     Val : val r ty -> Value r ty
     -- represent arrays as Vect
     VArr : Vect n (Value r ty) -> Value r (TArray ty n)
     -- empty record
     VRecNil : Value r (TRecord [])
     -- record field
     VRecCons : (name : String) -> -- proof name is not in fs
                -- {auto prfNL : So (isNothing (lookup name fs))} ->
                Value r ty ->
                Value r (TRecord fs) ->
                Value r (TRecord ((name, ty) :: fs))

-- TODO: do we need this?
public export
data ValFitsExp : Value r ty -> Type where
     ValFits_ValInt : ValFitsExp (ValInt i)
     ValFits_Op : ValFitsExp (VOp o)
     ValFits_Val : ValFitsExp (Val e)
