module Tapir.Arch

import Tapir.Type
import Tapir.Repr

%default total

public export
record Arch (r : Repr) where
       constructor MkArch
       wordSize : ISize
       IsSafePtr : Operand r (TInt wordSize) -> Type

public export
record PArch (r : Repr) where
  constructor MkPArch
  ArchParam : Type
  pArch : ArchParam -> Arch r

public export
Word : Arch r -> TType
Word arch = TInt (wordSize arch)
