module Tapir.FFI

import Data.Vect

import Tapir.Type
import Tapir.Function.Signature
import Tapir.Repr

public export
record FFI (r : Repr) where
  constructor MkFFI
  functions : Vect k (String, FunctionSignature r)
  globals : Vect l (String, TType)
