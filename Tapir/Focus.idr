module Tapir.Focus

import Data.Vect

import Tapir.Type
import Tapir.Repr
import Tapir.Proof
import Utils

public export
data Focus : (r : Repr) -> TType -> TType -> Type where
     FId : Focus r t t
     FCo : Focus r t t' -> Focus r t' t'' -> Focus r t t''
     FEl : {t : TType} ->
           Focus r t (TArray t' n) ->
           (i : Operand r (TInt ISZ64)) ->
           Proof r (LT i (Imm64 $ fromInteger $ cast n)) ->
           Focus r t t'
     FFld : Focus r t (TRecord fs) ->
            (name : String) ->
            {auto prfL : IsJust (lookup name fs)} ->
            Focus r t (getJust (lookup name fs) prfL)
