module Tapir.Effect

import Data.Vect

import Tapir.Type

public export
data Effect' : (reg : TType -> Type) -> (ref : TType -> Type) -> Type where
  EUpdate  : ref t -> Effect' reg ref
  EEPWrite : reg (TOpaque "tapirEP" [t]) -> Effect' reg ref
  EFork    : List (Effect' reg ref) -> List (Effect' reg ref) -> Effect' reg ref

