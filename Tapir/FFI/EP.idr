module Tapir.FFI.EP

import Data.Vect

import Tapir.Type
import Tapir.Effect
import Tapir.Function.Signature
import Tapir.Repr
import Tapir.FFI

import Utils

%default total

public export
epStartSig : FunctionSignature reg
epStartSig = MkFunSig
  TType
  TRef
  (\ty => [TOpaque "tapirEP" [ty]])
  (\_, _ => [])
  (\_ => 64)
  (\_ => 64)
  (\_, _ => [])

public export
epCommitSig : FunctionSignature reg
epCommitSig = MkFunSig
  TType
  (\ty => TInt ISZ64)
  (\ty => [TOpaque "tapirEP" [ty], TRef ty])
  (\_, _, _ => [])
  (\_ => 16)
  (\_ => 16)
  (\_, (Reg ep), _ => [EEPWrite ep])

public export
EpFFI : FFI r
EpFFI = MkFFI
  [ ("ep_start", epStartSig)
  , ("ep_commit", epCommitSig)
  ]
  []
