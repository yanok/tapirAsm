module Tapir.FFI.Empty

import Data.Vect

import Tapir.Type
import Tapir.Function.Signature
import Tapir.Repr
import Tapir.FFI

public export
EmptyFFI : FFI r
EmptyFFI = MkFFI [] []
