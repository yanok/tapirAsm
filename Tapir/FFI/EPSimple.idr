module Tapir.FFI.EPSimple

import Data.Vect

import Tapir.Type
import Tapir.Effect
import Tapir.Function.Signature
import Tapir.Repr
import Tapir.FFI

import Utils

%default total

public export
epPushSig : FunctionSignature r
epPushSig = MkFunSig
  TType
  (\ty => TInt ISZ64)
  (\ty => [TOpaque "tapirEP" [ty], ty])
  (\_, _, _ => [])
  (\_ => 64)
  (\ty => 128 + opSize ty)
  (\_, (Reg ep), _ => [EEPWrite ep])

public export
EpSimpleFFI : FFI r
EpSimpleFFI = MkFFI
  [ ("ep_push", epPushSig)
  ]
  []
