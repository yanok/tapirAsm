module Tapir.CInterop.ConvertType

import Tapir.Type
import Tapir.Repr
import Tapir.Arch
import MiniC.Syntax

-- TODO: probably also add struct and array
-- this doesn't work unfortunately: convertType doesn't reduce
-- so switch to Maybe approach
public export
data ConvertableCType : CType -> Type where
  ConvertableSigned : ConvertableCType (CSigned x)
  ConvertableUnsigned : ConvertableCType (CUnsigned x)
  ConvertablePtr : ConvertableCType (CPtr x)

public export
convertSize : CSize -> ISize
convertSize CSZ8 = ISZ8
convertSize CSZ16 = ISZ16
convertSize CSZ32 = ISZ32
convertSize CSZ64 = ISZ64

public export
convertType : (arch : Arch r) -> (ct : CType) -> Maybe TType
convertType arch (CSigned sz) = Just $ TInt (convertSize sz)
convertType arch (CUnsigned sz) = Just $ TInt (convertSize sz)
convertType arch (CPtr x) = Just $ Word arch
convertType _ _ = Nothing
