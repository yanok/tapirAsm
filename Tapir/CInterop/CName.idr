module Tapir.CInterop.CName

import Tapir.Type
import Tapir.Repr
import Tapir.Arch
import Tapir.CInterop.ConvertType
import MiniC.Syntax
import Utils

%default total

-- have to put type inside but it really belongs to CNameRep
-- unfortunately I can't force convertType to reduce if I use
-- it in CNameRep definition
-- this makes CName dependent on arch, unfortunately
-- do we need both cType and normCType?
public export
record CName (arch : Arch r) where
  constructor MkCName
  name : String
  cType : CType
  normCType : CType
  type : TType
  typeIsCorrect : convertType arch normCType = Just type

public export
record CNameRep (r : Repr) (arch : Arch r) (cName : CName arch) where
  constructor MkCNameRep
  nmReg : reg r (type cName)
