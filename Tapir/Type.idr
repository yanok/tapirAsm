module Tapir.Type

import Data.Vect

%default total

public export
data ISize : Type where
     ISZ8 : ISize
     ISZ16 : ISize
     ISZ32 : ISize
     ISZ64 : ISize

public export
data TType : Type where
     TInt : ISize -> TType
     TRef : TType -> TType
     TArray : TType -> Nat -> TType
     TRecord : Vect n (String, TType) -> TType
     TOpaque : String -> Vect n TType -> TType

public export
data Operand' : (reg : TType -> Type) -> (ref : TType -> Type) -> TType -> Type where
     Imm8 : Bits8 -> Operand' reg ref (TInt ISZ8)
     Imm16 : Bits16 -> Operand' reg ref (TInt ISZ16)
     Imm32 : Bits32 -> Operand' reg ref (TInt ISZ32)
     Imm64 : Bits64 -> Operand' reg ref (TInt ISZ64)
     Reg : reg t -> Operand' reg ref t
     Ref : ref t -> Operand' reg ref (TRef t)

public export
data Prop' : (reg : TType -> Type) -> (ref : TType -> Type) -> Type where
     EQL : Operand' reg ref (TInt sz) -> Operand' reg ref (TInt sz) -> Prop' reg ref
     LT  : Operand' reg ref (TInt sz) -> Operand' reg ref (TInt sz) -> Prop' reg ref
     NOT : Prop' reg ref -> Prop' reg ref

syntax "%" [r] = Reg r
syntax "@" [v] = Imm64 v

public export
size : TType -> Nat
size (TInt ISZ8) = 1
size (TInt ISZ16) = 2
size (TInt ISZ32) = 4
size (TInt ISZ64) = 8
size (TRef _) = 8
size (TArray ty n) = n * size ty
size (TRecord fs) = sizes fs where
  sizes : Vect n (String, TType) -> Nat
  sizes [] = 0
  sizes ((_, t) :: fs) = size t + sizes fs
size (TOpaque _ _) = 8 -- opaque types are pointers, TODO: should be arch-dependent

public export
opSize : TType -> Nat
opSize (TInt _) = 1
opSize (TRef _) = 1
opSize (TArray ty n) = n * opSize ty
opSize (TRecord fs) = opSizes fs where
  opSizes : Vect n (String, TType) -> Nat
  opSizes [] = 0
  opSizes ((_, t) :: fs) = opSize t + opSizes fs
opSize (TOpaque _ _) = 1
