module Loopholes

import Types
import Stages

public export
data LoopHole : (reg : TType -> Type) -> {ty : TType} -> reg ty -> Type where
     MkHole : String -> (r : reg ty) -> LoopHole reg r
