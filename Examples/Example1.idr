module Examples.Example1

import Tapir.Arch
import Tapir.Arch.X86_64 as X86_64
import Tapir.Type
import Tapir.Repr
import Tapir.Proof
import Tapir.Value
import Tapir.Instruction
import Tapir.Effect

import Examples.SysInfoExample

import Tapir.CCodeGen
import Tapir.CCodeGen.CGRepr
import MiniC.Syntax
import MiniC.Pretty
import Text.PrettyPrint.WL

MyArch : Arch r
MyArch = pArch X86_64.x8664PArch exampleSysInfo

example : Instruction r MyArch [] (31 + s) s (10 + st) st ()
example = do
        r0 <- peek ISZ64 (@ 0xff80000000000000)
        r1 <- div (% r0) (@ 2)
        b <- eql (% r0) (@ 0)
        r2 <- br b
                 (do {
                 Wait 1
                 Pure $ ValInt {sz = ISZ64} 0
                 })
                 ( Div (% r0) (% r0) )
        ch <- lt (% r0) (@ (memHighBound exampleSysInfo))
        cl <- lt (@ (memLowBound exampleSysInfo)) (% r0)
        r5 <- br ch (do {
                 r6 <- br cl ( Peek ISZ16 (% r0) )
                             (do {
                               Wait 1
                               Pure $ ValInt {sz = ISZ16} 1
                             })
                 Pure $ VOp (% r6)
                 })
                 (do {
                 Waste 2
                 Wait 2
                 Pure $ ValInt {sz = ISZ16} 2
                 })
        Pure ()

cgExample : Instruction CGRepr MyArch [] 31 0 10 0 ()
cgExample = example

test : IO ()
test = putStrLn (toString $ prBlock $ codegenInstruction cgExample)
