module Examples.StaticPeek

import Tapir.Arch
import Tapir.Arch.X86_64 as X86_64
import Tapir.Type
import Tapir.Repr
import Tapir.Proof
import Tapir.Value
import Tapir.Instruction
import Tapir.Effect

import Examples.SysInfoExample

MyArch : Arch r
MyArch = pArch X86_64.x8664PArch exampleSysInfo

example : Instruction r MyArch [] (8 + s) s (1 + st) st ()
example = do
        peek ISZ64 (@ 0xff80000000000000)
        Pure ()
