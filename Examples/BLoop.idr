module Examples.BLoop

import Tapir.Arch
import Tapir.Arch.X86_64 as X86_64
import Tapir.Type
import Tapir.Repr
import Tapir.Proof
import Tapir.Value
import Tapir.Instruction
import Tapir.Effect

import Examples.SysInfoExample

import Tapir.CCodeGen
import Tapir.CCodeGen.CGRepr
import MiniC.Syntax
import MiniC.Pretty
import Text.PrettyPrint.WL

MyArch : Arch r
MyArch = pArch X86_64.x8664PArch exampleSysInfo

example : Instruction r MyArch [] (40 + s) (16 + s) (32 + st) st ()
example = do
        r0 <- peek ISZ64 (@ 0xff80000000000000)
        r1 <- div (% r0) (@ 2)
        r3 <- BLoop {st = 3} {s' = 8} {n = 10} (@ 10) (% r1) (\c, p, acc, h => do
              r2 <- peek ISZ64 (@ 0xff80000000000000)
              v <- Add (% acc) (% r2)
              Break v h
              Pure v
        )
        Pure ()

cgExample : Instruction CGRepr MyArch [] 40 16 32 0 ()
cgExample = example

test : IO ()
test = putStrLn (toString $ prBlock $ codegenInstruction cgExample)
