module Examples.EPExample

import Data.Vect
import Data.HVect

import Tapir.Arch
import Tapir.Arch.X86_64 as X86_64
import Tapir.Type
import Tapir.Repr
import Tapir.Proof
import Tapir.Value
import Tapir.Instruction
import Tapir.CInterop.CName
import Tapir.CInterop.ConvertType
import Tapir.FFI
import Tapir.FFI.EP
import Tapir.Module
import Tapir.Function.Signature
import Tapir.Focus
import Tapir.Effect
import MiniC.Syntax
import Utils

import Examples.SysInfoExample

import Tapir.CCodeGen
import Tapir.CCodeGen.CGRepr
import MiniC.Pretty
import Text.PrettyPrint.WL

MyArch : Arch r
MyArch = pArch X86_64.x8664PArch exampleSysInfo

fildes : CName MyArch
fildes = MkCName "fildes" (CSigned CSZ32) (CSigned CSZ32) (TInt ISZ32) Refl

ptr : {r : Repr} -> CName {r} MyArch
ptr {r} = MkCName "ptr" (CPtr CVoid) (CPtr CVoid) (Word {r} MyArch) Refl

nbyte : CName MyArch
nbyte = MkCName "nbyte" (CTypedef "size_t") (CUnsigned CSZ64) (TInt ISZ64) Refl

traceRec : TType
traceRec = TRecord [("tag", TInt ISZ64), ("data", TInt ISZ64)]

probe : {r : Repr} -> Module r MyArch EpFFI ()
probe {r} = do
  ([epStart, epCommit],[]) <- GetFFI
  rcrd <- Data "record" $ TRecord [("counter", TInt ISZ64)]
  ep <- AllocEP "trace" traceRec
  JProbe "sys_read" [fildes,ptr,nbyte] [EEPWrite ep, EUpdate rcrd] 112 85
     (\fd, p, n => do
         -- getting strange error without explicit proof...
         c <- ReadRef (Ref rcrd) (FFld FId "counter" {prfL = ItIsJust})
         rc <- Alloca c
         rv <- add (% rc) (% (nmReg n))
         buf' <- Call epStart traceRec [% ep] {prfs = []}
         -- this shouldn't be needed... seems like an Idris bug
         let buf = the (reg r (TRef traceRec)) buf'
         WriteRef (% buf) (FFld FId "tag" {prfL = ItIsJust}) (@ 0)
         WriteRef (% buf) (FFld FId "data" {prfL = ItIsJust}) (% rv)
         -- have to pass the empty(!) proof list explicitly
         Call epCommit traceRec [% ep, % buf] {prfs = []}
         -- and also here
         WriteRef (Ref rcrd) (FFld FId "counter" {prfL = ItIsJust}) (% rv)
         Pure ()
         )

cgProbe : Module CGRepr MyArch EpFFI ()
cgProbe = probe

test : IO ()
test = putStrLn (toString $ prModule $ codegenModule cgProbe)
