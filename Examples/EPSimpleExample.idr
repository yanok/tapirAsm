module Examples.EPSimpleExample

import Data.Vect
import Data.HVect

import Tapir.Arch
import Tapir.Arch.X86_64 as X86_64
import Tapir.Type
import Tapir.Repr
import Tapir.Proof
import Tapir.Value
import Tapir.Instruction
import Tapir.CInterop.CName
import Tapir.CInterop.ConvertType
import Tapir.FFI
import Tapir.FFI.EPSimple
import Tapir.Module
import Tapir.Function.Signature
import Tapir.Focus
import Tapir.Effect
import MiniC.Syntax
import Utils

import Examples.SysInfoExample

import Tapir.CCodeGen
import Tapir.CCodeGen.CGRepr
import MiniC.Pretty
import Text.PrettyPrint.WL

MyArch : Arch r
MyArch = pArch X86_64.x8664PArch exampleSysInfo

fildes : CName MyArch
fildes = MkCName "fildes" (CSigned CSZ32) (CSigned CSZ32) (TInt ISZ32) Refl

ptr : {r : Repr} -> CName {r} MyArch
ptr {r} = MkCName "ptr" (CPtr CVoid) (CPtr CVoid) (Word {r} MyArch) Refl

nbyte : CName MyArch
nbyte = MkCName "nbyte" (CTypedef "size_t") (CUnsigned CSZ64) (TInt ISZ64) Refl

traceRec : TType
traceRec = TRecord [("tag", TInt ISZ64), ("data", TInt ISZ64)]

probe : {r : Repr} -> Module r MyArch EpSimpleFFI ()
probe {r} = do
  ([epPush],[]) <- GetFFI
  rcrd <- Data "record" $ TRecord [("counter", TInt ISZ64)]
  ep <- AllocEP "trace" traceRec
  JProbe "sys_read" [fildes,ptr,nbyte] [EEPWrite ep, EUpdate rcrd] 104 133
     (\fd, p, n => do
         -- getting strange error without explicit proof...
         c <- ReadRef (Ref rcrd) (FFld FId "counter" {prfL = ItIsJust})
         rc <- Alloca c
         rv <- add (% rc) (% (nmReg n))
         let recV = VRecCons "tag" (ValInt 0) (VRecCons "data" (VOp (% rv)) VRecNil)
         buf <- Alloca recV
         -- have to pass the empty(!) proof list explicitly
         Call epPush traceRec [% ep, % buf] {prfs = []}
         -- and also here
         WriteRef (Ref rcrd) (FFld FId "counter" {prfL = ItIsJust}) (% rv)
         Pure ()
         )

test : IO ()
test = putStrLn (toString $ prModule $ codegenModule probe)
