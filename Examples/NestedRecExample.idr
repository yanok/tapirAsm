module Examples.NestedRecExample

import Data.Vect
import Data.HVect

import Tapir.Arch
import Tapir.Arch.X86_64 as X86_64
import Tapir.Type
import Tapir.Repr
import Tapir.Proof
import Tapir.Value
import Tapir.Instruction
import Tapir.CInterop.CName
import Tapir.CInterop.ConvertType
import Tapir.FFI
import Tapir.FFI.Empty
import Tapir.Module
import Tapir.Function.Signature
import Tapir.Focus
import Tapir.Effect
import MiniC.Syntax
import Utils

import Examples.SysInfoExample

import Tapir.CCodeGen
import Tapir.CCodeGen.CGRepr
import MiniC.Pretty
import Text.PrettyPrint.WL

MyArch : Arch r
MyArch = pArch X86_64.x8664PArch exampleSysInfo

fildes : CName MyArch
fildes = MkCName "fildes" (CSigned CSZ32) (CSigned CSZ32) (TInt ISZ32) Refl

ptr : {r : Repr} -> CName {r} MyArch
ptr {r} = MkCName "ptr" (CPtr CVoid) (CPtr CVoid) (Word {r} MyArch) Refl

nbyte : CName MyArch
nbyte = MkCName "nbyte" (CTypedef "size_t") (CUnsigned CSZ64) (TInt ISZ64) Refl

rec2 : TType
rec2 = TRecord
     [ ("x", TInt ISZ64)
     , ("y", TInt ISZ64)
     ]

rec1 : TType
rec1 = TRecord
     [ ("tag", TInt ISZ64)
     , ("data", rec2)
     ]

probe : Module r MyArch EmptyFFI ()
probe = do
  ([],[]) <- GetFFI
  rcrd <- Data "record" rec1
  JProbe "sys_read" [fildes,ptr,nbyte] [EUpdate rcrd] 16 3 (\fd, p, n => do
         -- have to define our focus beforehand with the explicit type
         let f = the (Focus _ rec1 (TInt ISZ64))
                     (FFld (FFld FId "data") "x")
         c <- ReadRef (Ref rcrd) f
         rc <- Alloca  c
         rv <- add (% rc) (% (nmReg n))
         WriteRef (Ref rcrd) f (% rv)
         Pure ()
         )

test : IO ()
test = putStrLn (toString $ prModule $ codegenModule probe)
