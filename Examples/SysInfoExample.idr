module Example.SysInfoExample

import Tapir.Arch.X86_64

public export
exampleSysInfo : X8664SysInfo
exampleSysInfo = MkX8664SysInfo
  -- memHighBound
  -- right now we check only the pointer itself, not pointer + size, so make it
  -- smaller, unfortunately it makes upper 8 bytes only accessible as a whole
  0xffbffffffffffff9
  -- memLowBound
  0xff7fffffffffffff
