(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "http://stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(package-initialize)
(unless (package-installed-p 'org-plus-contrib)
  (package-refresh-contents)
  (package-install 'org-plus-contrib))
(unless (package-installed-p 'org-ref)
  (package-refresh-contents)
  (package-install 'org-ref))
(require 'org)
(require 'ox)
(require 'org-ref)

;; configuration

(setq org-latex-listings 'minted)
(setq org-latex-packages-alist '(("" "minted" nil)))
(setq org-latex-pdf-process '("latexmk -f -pdf -shell-escape %f"))
(setq org-export-allow-bind-keywords t) ; allow #+BIND needed to delay \maketitle
;; add acmart class
(add-to-list 'org-latex-classes
             '("acmart"
               "\\documentclass{acmart}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
             )

;; ignore_heading tag in Org mode, based on the manual and func docs
(defun ignored-headlines-removal (backend)
  "Remove all headlines with tag ignore_heading in the current buffer.
     BACKEND is the export back-end being used, as a symbol."
  (org-map-entries
   (lambda () (delete-region (point) (progn (forward-line) (point))))
   "ignore_heading"))

(add-hook 'org-export-before-parsing-hook 'ignored-headlines-removal)
(setq org-latex-prefer-user-labels t)
