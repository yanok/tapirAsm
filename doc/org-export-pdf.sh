#!/bin/sh

EMACS=${EMACS:-emacs}

$EMACS --batch -l export.el --visit $1 -f org-latex-export-to-pdf

