module Prelude.Pair.Extra

public export
Foldable (Pair a) where
  foldr func init (a, b) = func b init
  foldl func init (a, b) = func init b

public export
Traversable (Pair a) where
  traverse f (a, b) = (\x => (a,x)) <$> f b
