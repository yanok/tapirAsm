module MiniC.Syntax

%access public export

-- we don't support arch-dependent sizes in MiniC
data CSize = CSZ8 | CSZ16 | CSZ32 | CSZ64
data CType = CVoid
           | CSigned CSize
           | CUnsigned CSize
           | CPtr CType
           | CStruct String
           | CAnonStruct (List (String, CType))
           | CTypedef String
           | CArray CType Nat

implementation Eq CSize where
  CSZ8  == CSZ8  = True
  CSZ16 == CSZ16 = True
  CSZ32 == CSZ32 = True
  CSZ64 == CSZ64 = True
  _     == _     = False

implementation Ord CSize where
  compare CSZ8  CSZ8  = EQ
  compare CSZ8  _     = LT
  compare CSZ16 CSZ8  = GT
  compare CSZ16 CSZ16 = EQ
  compare CSZ16 _     = LT
  compare CSZ32 CSZ32 = EQ
  compare CSZ32 CSZ64 = LT
  compare CSZ32 _     = GT
  compare CSZ64 CSZ64 = EQ
  compare CSZ64 _     = LT

implementation Eq CType where
  CVoid          == CVoid          = True
  (CSigned x)    == (CSigned y)    = x == y
  (CUnsigned x)  == (CUnsigned y)  = x == y
  (CPtr x)       == (CPtr y)       = x == y
  (CStruct x)    == (CStruct y)    = x == y
  -- have to inline (==) for List to please the termination checker
  (CAnonStruct xs) == (CAnonStruct ys) = helper xs ys where -- x == y && xs == ys
    helper []           []           = True
    helper ((s1,x)::xs) ((s2,y)::ys) = s1 == s2 && x == y && helper xs ys
    helper _            _            = False
  (CTypedef x)   == (CTypedef y)   = x == y
  (CArray x k)   == (CArray y l)   = x == y && k == l
  _              == _              = False

implementation Ord CType where
  compare CVoid          CVoid          = EQ
  compare CVoid          _              = LT
  compare (CSigned _)    CVoid          = GT
  compare (CSigned x)    (CSigned y)    = compare x y
  compare (CSigned _)    _              = LT
  compare (CUnsigned _)  CVoid          = GT
  compare (CUnsigned _)  (CSigned _)    = GT
  compare (CUnsigned x)  (CUnsigned y)  = compare x y
  compare (CUnsigned _)  _              = LT
  compare (CPtr _)       CVoid          = GT
  compare (CPtr _)       (CSigned _)    = GT
  compare (CPtr _)       (CUnsigned _)  = GT
  compare (CPtr x)       (CPtr y)       = compare x y
  compare (CPtr _)       _              = LT
  compare (CStruct _)    (CAnonStruct _) = LT
  compare (CStruct _)    (CTypedef _)   = LT
  compare (CStruct _)    (CArray _ _)   = LT
  compare (CStruct x)    (CStruct y)    = compare x y
  compare (CStruct _)    _              = GT
  compare (CAnonStruct _)  (CTypedef _) = LT
  compare (CAnonStruct _)  (CArray _ _) = LT
  compare (CAnonStruct xs) (CAnonStruct ys) = helper xs ys where
    -- same stuff
    helper []           []           = EQ
    helper []           _            = LT
    helper _            []           = GT
    helper ((s1,x)::xs) ((s2,y)::ys)
      = compare s1 s2 `thenCompare` (compare x y `thenCompare` helper xs ys)
  compare (CAnonStruct _) _             = GT
  compare (CTypedef _)   (CArray _ _)   = LT
  compare (CTypedef x)   (CTypedef y)   = compare x y
  compare (CTypedef _)   _              = GT
  compare (CArray x k)   (CArray y l)   = compare x y `thenCompare` compare k l
  compare (CArray _ _)   _              = GT

data CExp = CAdd CExp CExp
          | CSub CExp CExp
          | CMul CExp CExp
          | CDiv CExp CExp
          | CEql CExp CExp
          | CNeq CExp CExp
          | CLt CExp CExp
          | CAnd CExp CExp
          | COr CExp CExp
          | CNot CExp
          | CBAnd CExp CExp
          | CBOr CExp CExp
          | CBXor CExp CExp
          | CBNeg CExp
          | CLShift CExp CExp
          | CRShift CExp CExp
          | CIntLit Integer
          | CDeref CType CExp
          | CIndex CExp CExp
          | CVar String
          | CFunCall String (List CExp)
          | CField CExp String
          | CAddrOf CExp
          | CCast CType CExp

mutual

  data CDef = CTypeDef String CType
            | CStructDef (List (String, CType)) String
            | CVarDef String CType (Maybe CExp)
            | CFunDef CType String (List (String, CType)) CBlock

  -- data ValidInFunction : CDef -> Type where
  --      CTypeDefValid : ValidInFunction (CTypeDef name t)
  --      CVarDefValid : ValidInFunction (CVarDef name t)

  CBlock : Type
  CBlock = (List CDef, List (Maybe String, CStmt))

  data CStmt = CAssign CExp CExp
             | CIf CExp CBlock CBlock
             | CWhile CExp CBlock
             | CDiscard CExp
             | CReturn CExp
             | CGoto String

implementation Semigroup CBlock where
  (<+>) (xdefs, xs) (ydefs, ys) = (xdefs <+> ydefs, xs <+> ys)
implementation Monoid CBlock where
  neutral = ([], [])

CModule : Type
CModule = List CDef
