module MiniC.Pretty

import Text.PrettyPrint.WL

import MiniC.Syntax

sizeToInt : CSize -> Int
sizeToInt CSZ8 = 8
sizeToInt CSZ16 = 16
sizeToInt CSZ32 = 32
sizeToInt CSZ64 = 64

export
prType : CType -> Doc
prType CVoid = text "void"
prType (CSigned sz) = text "int" |+| int (sizeToInt sz) |+| text "_t"
prType (CUnsigned sz) = text "uint" |+| int (sizeToInt sz) |+| text "_t"
prType (CPtr t) = prType t |++| char '*'
prType (CStruct x) = text "struct" |++| text x
prType (CAnonStruct xs) = text "struct" |++|
       semiBrace (map (\ (nm, ty) => prType ty |++| text nm) xs)
prType (CTypedef x) = text x
prType (CArray t n) = prType t |+| brackets (nat n)

export
prExp : CExp -> Doc
prExp (CAdd x y) = parens (prExp x) |++| char '+' |++| parens (prExp y)
prExp (CSub x y) = parens (prExp x) |++| char '-' |++| parens (prExp y)
prExp (CMul x y) = parens (prExp x) |++| char '*' |++| parens (prExp y)
prExp (CDiv x y) = parens (prExp x) |++| char '/' |++| parens (prExp y)
prExp (CEql x y) = parens (prExp x) |++| text "==" |++| parens (prExp y)
prExp (CNeq x y) = parens (prExp x) |++| text "!=" |++| parens (prExp y)
prExp (CLt x y) = parens (prExp x) |++| char '<' |++| parens (prExp y)
prExp (CAnd x y) = parens (prExp x) |++| text "&&" |++| parens (prExp y)
prExp (COr x y) = parens (prExp x) |++| text "||" |++| parens (prExp y)
prExp (CNot x) = char '!' |+| parens (prExp x)
prExp (CBAnd x y) = parens (prExp x) |++| char '&' |++| parens (prExp y)
prExp (CBOr x y) = parens (prExp x) |++| char '|' |++| parens (prExp y)
prExp (CBXor x y) = parens (prExp x) |++| char '^' |++| parens (prExp y)
prExp (CBNeg x) = char '~' |+| parens (prExp x)
prExp (CIntLit x) = integer x
prExp (CDeref x y) = asterix |++| parens (prExp y)
prExp (CIndex a i) = parens (prExp a) |+| brackets (prExp i)
prExp (CVar x) = text x
prExp (CFunCall x xs) = text x |+| tupled (map prExp xs)
prExp (CField e f) = parens (prExp e) |+| char '.' |+| text f
prExp (CAddrOf e) = char '&' |+| parens (prExp e)
prExp (CCast t e) = parens (prType t) |+| parens (prExp e)

export
prTypeName : CType -> Doc -> Doc
prTypeName (CArray ty n) nm = prTypeName ty (nm |+| brackets (nat n))
prTypeName t nm = prType t |++| nm

export
prDef : CDef -> Doc
export
prStmt : CStmt -> Doc
export
prBlock : CBlock -> Doc
export
prLStmt : (Maybe String, CStmt) -> Doc

prDef (CTypeDef nm t) = text "typedef" |++| prTypeName t (text nm) |+| semi
prDef (CStructDef fs nm) = text "struct" |++| text nm |++| lbrace |$$| indent 2 (
      vcat (map (\ (nm, ty) => prType ty |++| text nm |+| semi) fs)
      ) |$$| rbrace |+| semi
prDef (CVarDef v t Nothing) = prTypeName t (text v) |+| semi
prDef (CVarDef v t (Just e)) = prTypeName t (text v) |++| equals |++| prExp e |+| semi
prDef (CFunDef rt f args block) = prType rt |++| text f |+|
      tupled (map (\ (nm, ty) => prType ty |++| text nm) args) |$$|
      prBlock block

prStmt (CAssign x y) = prExp x |++| equals |++| prExp y |+| semi
prStmt (CIf x y z) = text "if" |++| parens (prExp x) |$$|
                          prBlock y |++| text "else" |++|
                          prBlock z
prStmt (CDiscard x) = prExp x |+| semi
prStmt (CReturn x) = text "return" |++| prExp x |+| semi
prStmt (CWhile e b) = text "while" |++| parens (prExp e) |$$| prBlock b
prStmt (CGoto l) = text "goto" |++| text l |+| semi

prLStmt (Nothing, s) = prStmt s
prLStmt (Just l, s) = text l |+| colon |++| prStmt s

prBlock (defs, ss) = lbrace |$$| indent 2 (
                        vcat (map prDef defs) |$$|
                        empty |$$|
                        vcat (map prLStmt ss))
                 |$$| rbrace

export
prModule : CModule -> Doc
prModule = vcat . map prDef
