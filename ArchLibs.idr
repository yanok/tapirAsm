module ArchLibs

import Stages
import Archs
import Types
import Syntax

public export
record ArchLib (arch : ArchDef) (stage : Stage) where
  constructor MkArchLib
  withSafePtrStepCost : Nat
  withSafePtrStackCost : Nat
  withSafePtr : {s, s', st, st' : Nat}
              -> {a : Type}
              -> (p : Operand stage (Word arch))
              -> ((prf : IsSafePtr arch p) -> Instr arch stage s s' st st' a)
              -> Instr arch stage s s' st st' a
              -> Instr arch stage (withSafePtrStackCost + s) s'
                                  (withSafePtrStepCost + st) st' a
